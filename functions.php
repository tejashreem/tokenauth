<?php

    function getToken()
    {
        if (!isset($_SESSION['user_token']))
        {
            $_SESSION['user_token'] = md5(uniqid());
        }
    }
    
    function verifyToken($token)
    {
        if ($token != $_SESSION['user_token'])
        {
            header('location: 404.php');
            exit;
        }
    }
    
    function getTokenField()
    {
        return '<input type="hidden" name="secure_token" value="'.$_SESSION['user_token'].'" />';
    }
    
    function destroyToken()
    {
        unset($_SESSION['user_token']);
    }
?>